I appreciate the lack of tracked absences, as it provides greater flexibility in my already busy schedule.

I could do with less sighs and eyerolls when we as a class don't have an answer to one of Irina's questions, it made me feel uneasy.

I learned a few things snippets of new information about tags or the meaning of ^ or the layers of git's configs,
but as a self-proclaimed expert at git (which learned many things about git during his RedHat internship),
from my point of view it shouldn't be called Mastering Git, more like Learning Git from the beginning - the first two lessons moved very slow for me.

Hopefully you can appreciate my honest two cents coming from the heart.
